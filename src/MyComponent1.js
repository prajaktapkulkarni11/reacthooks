import { useContext } from "react"
import { UserContext } from "./UserContext"
// import {ContextValue} from './App'



function MyComponent1(){
    const msg=useContext(UserContext)
    return(
        <div>
            <h2>MyComponent1</h2>
            <p>Context Value: {msg}</p>
        </div>
    )
}

export default MyComponent1