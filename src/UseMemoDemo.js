import {useMemo, useState} from 'react'

const data=[{
    "id":1,
    "name":"ABC"
},{
    "id":2,
    "name":"PQR"
},{
    "id":3,
    "name":"XYZ"
},{
    "id":4,
    "name":"LMN"
}]

function UseMemoDemo(){

    const[text,setText]=useState('')
    const[keyword,setKeyword]=useState('')

    const onChange=(e)=>{
        setText(e.target.value)
    }

    const handleChange=()=>{
        setKeyword(text)
    }
    
    const filterData=useMemo(()=>data.filter((record)=>{
        console.log("Checking");
        return record.name.includes(keyword)
    }),[keyword]) 

    

    

    return(
        <div>
            <input type="text" name="search" value={text} onChange={onChange}/>
            <button type="button" onClick={handleChange}>Search</button>
            <MyList data={filterData}/>
            
        </div>
    )
}

function MyList({data}){
    
    return(
        <div>
            <ul>
                {data.map((item)=>(
                    <Item key={item.id} item={item}/>
                ))}
            </ul>
        </div>
    )
}

function Item({item}){
    return(
        <div>
            <li>{item.name}</li>
        </div>
    )
}

export default UseMemoDemo