import { useContext } from "react"
import { UserContext } from "./UserContext"

function MyComponent4(){
    const msg=useContext(UserContext)
    return(
        <div>
            <h2>MyComponent4</h2>
            <p>Context Value: {msg}</p>
        </div>
    )
}

export default MyComponent4