import React from 'react';
import ReactDOM from 'react-dom';
 import './index.css';
 import App from './App';
 import reportWebVitals from './reportWebVitals';
// import MyApp from './MyApp'
// import Student from './Student'
// import StudentName from './StudentName'
// import MyApp2 from './MyApp2'

// function MyApp1(){
//   return(
//     <div>
//       <h1>MyApp1</h1>
//       <p>MyApp1 paragraph</p>
//     </div>
//   )
// }

ReactDOM.render(
  
  <div>
  {/* <MyApp/>
  <MyApp1/>
  <Student name="Prajakta"/>
  <StudentName name="ABC"/> */}
  {/* <MyApp2/> */}

  <App/>
  </div>
  ,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
