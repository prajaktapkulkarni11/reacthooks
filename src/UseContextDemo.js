import React, { useContext } from 'react'

const ContextValue=React.createContext()

function UseContextDemo(){
    const msg="hello"
    return(
        <ContextValue.Provider value={100}>
            <DisplayContextValue/>
        </ContextValue.Provider>
    )
}

function DisplayContextValue(){
    const value=useContext(ContextValue)
    return(
        // <ContextValue.Consumer>
        //     {value=><p>Context Value is: {value}</p>}
        // </ContextValue.Consumer>

        <p>Context value is: {value}</p>
    )
}

export default UseContextDemo