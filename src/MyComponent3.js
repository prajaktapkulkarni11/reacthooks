import { useContext } from "react"
import { UserContext } from "./UserContext"

function MyComponent3(){
    const msg=useContext(UserContext)
    return(
        <div>
            <h2>MyComponent3</h2>
            <p>Context Value: {msg}</p>
        </div>
    )
}

export default MyComponent3