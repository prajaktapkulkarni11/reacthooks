import {useReducer} from 'react'

const sampleData=[{
    "id":1,
    "taskName":"Learn Java",
    "status":false
},{
    "id":2,
    "taskName":"Learn React",
    "status":false
},{
    "id":3,
    "taskName":"Learn Java 8",
    "status":false
}]

const reducer=(state,action)=>{
    switch(action.type) {
        case 'CHECK':
            console.log("In check");
            return state.map(data=>{
                if(data.id===action.id){
                return {...data,status:true}
                }else{
                    return data
                }
            })
        case 'UNCHECK':
            console.log("In Uncheck");
            return state.map(data=>{
                if(data.id===action.id){
                    return {...data,status:false}
                }else{
                    return data
                }
            })
    
        default:
            return state
    }
}

function CheckUseReducerDemo(){

    const[taskData,dispatch]=useReducer(reducer,sampleData)

    const handleChange=data=>{
        dispatch({
            type:data.status? 'UNCHECK':'CHECK',
            id:data.id
        })
    }
    return(
        <div>
            <ul>
                {taskData.map(data=>(
                    <li key={data.id}>{data.taskName}
                        <label>
                            <input type="checkbox" checked={data.status} onChange={()=>handleChange(data)}/>
                        </label>
                    </li>
                ))}
            </ul>

        </div>
    )
}

export default CheckUseReducerDemo