// import { createContext } from 'react';
import './App.css';
import CheckUseReducerDemo from './CheckUseReducerDemo';
import UserContext from './UserContext'
import MyComponent1 from './MyComponent1';
import MyComponent2 from './MyComponent2';
import MyComponent3 from './MyComponent3';
import MyComponent4 from './MyComponent4';
// import UseReducerDemo from './UseReduceDemo';
// import UseMemoDemo from './UseMemoDemo';

import UseContextDemo from './UseContextDemo';

// export const ContextValue=createContext()
function App() {
  return (
    <div>
       {/* <UseContextDemo/> */}
       
       
       <UserContext.Provider value="Hello from App Component">
          <MyComponent1/>
          <MyComponent2/>
          <MyComponent3/>
          <MyComponent4/>
       </UserContext.Provider>


       {/* <UseReducerDemo/> */}

       {/* <UseMemoDemo/> */}

       {/* <CheckUseReducerDemo/> */}
    </div>
  );
}

export default App;
