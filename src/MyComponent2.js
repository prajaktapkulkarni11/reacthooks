import { useContext } from "react"
import { UserContext } from "./UserContext"

function MyComponent2(){
    const msg=useContext(UserContext)
    return(
        <div>
            <h2>MyComponent2</h2>
            <p>Context Value: {msg}</p>
        </div>
    )
}

export default MyComponent2